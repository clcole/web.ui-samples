
"use strict";

class Cards {
  constructor() {
    this.container = document.querySelector(".container");

    // querySelectorAll returns a NodeList, convert it into an Array
    // this.cards = Array.prototype.slice.call(document.querySelectorAll(".container__card"));
    this.cards = Array.from(document.querySelectorAll(".container__card"));

    this.target = {};
    this.startX = 0;
    this.currentX = 0;
    this.changeX = 0;
    this.finalX = 0;
    this.isTouching = false;

    this.addStartEventListeners();
  }

  addStartEventListeners = () => {
    for (const card of this.cards) {
      card.addEventListener("touchstart", this.onStart);
    }
  };

  onStart = (e) => {
    if (this.target.el) return;

    this.target.el = e.target;
    this.target.index = this.cards.indexOf(this.target.el);
    this.target.rectObj = this.target.el.getBoundingClientRect();
    this.target.styleObj = window.getComputedStyle(this.target.el);
    // console.log(this.target);

    this.startX = e.touches[0].pageX;
    this.currentX = this.startX;

    this.isTouching = true;
    this.target.el.style.willChange = "transform";

    this.target.el.addEventListener("touchmove", this.onMove);
    this.target.el.addEventListener("touchend", this.onEnd);

    // start the animation
    window.requestAnimationFrame(this.updateTouchedCard);

    // console.log("touchstart:", this.startX);
  };

  onMove = (e) => {
    if (!this.target.el) return;

    this.currentX = e.touches[0].pageX;

    // console.log("touchmove:", this.currentX);
  };

  onEnd = () => {
    if (!this.target.el) return;

    this.finalX = 0;
    this.changeX = this.currentX - this.startX;

    if (Math.abs(this.changeX) > (this.target.rectObj.width * 0.4)) {
      this.finalX = (this.changeX > 0) ? this.target.rectObj.width : -this.target.rectObj.width;
    }

    this.isTouching = false;

    this.target.el.removeEventListener("touchmove", this.onMove);
    this.target.el.removeEventListener("touchend", this.onEnd);

    // console.log("touchend:", this.currentX);
  };

  updateTouchedCard = () => {
    if (!this.target.el) return;

    if (this.isTouching) {
      this.changeX = this.currentX - this.startX;
    }
    else {
      // slow down how fast a card leaves or returns
      // changeX is where we are, finalX is where we want to be
      // divide the difference and add it to changeX each time through
      this.changeX += (this.finalX - this.changeX) / 4;
    }

    // slow down the opacity fade
    const swipeRatio = Math.abs(this.changeX) / this.target.rectObj.width;
    const swipeRatioCurved = Math.pow(swipeRatio, 3);
    const opacity = 1 - swipeRatioCurved;

    this.target.el.style.transform = `translateX(${this.changeX}px)`;
    this.target.el.style.opacity = opacity;

    // continue the animation until the touch has ended
    window.requestAnimationFrame(this.updateTouchedCard);

    // still touching, exit updateTouchedCard
    if (this.isTouching) return;

    // close to the beginning, reset target
    if (Math.abs(this.changeX) < 0.1) {
      this.resetTarget();
      return;
    }

    // close to the end, remove target
    // if (Math.abs(this.finalX - this.changeX) < 0.1) {
    if ((this.target.rectObj.width - Math.abs(this.changeX)) < 0.1) {
      this.clearTarget();
      return;
    }
  };

  resetTarget = () => {
    if (!this.target.el) return;

    this.target.el.style.transform = "none";
    this.target.el.style.willChange = "initial";
    this.target = {};
  };

  clearTarget = () => {
    if (!this.target.el || !this.target.el.parentNode) return;

    // gather some final info on the target before it is removed
    const targetIndex = this.target.index;
    const targetHeight = this.target.rectObj.height

    // remove the pixel unit 
    const targetBottomMargin = this.target.styleObj.getPropertyValue("margin-bottom").replace(/px/i, "");

    const targetGap = parseInt(targetHeight, 10) + parseInt(targetBottomMargin, 10);

    // remove the target from the list of cards
    this.cards.splice(targetIndex, 1);
    
    // remove the target from the dom
    // creates a gap that the lower cards will immediately try to move up to fill
    this.target.el.parentNode.removeChild(this.target.el);

    this.resetTarget();

    // maintain the gap and use a transition on the lower cards to fill it
    this.updateRemainingCards(targetIndex, targetGap);
  };

  updateRemainingCards = (startIndex, gapSize) => {
    // 3. clean up after the transition is done
    const onAnimationEnd = (e) => {
      const card = e.target;
      card.style.transform = "";
      card.style.transition = "";
      card.removeEventListener("transitionend", onAnimationEnd);
    };

    // 1. immediately hold the lower cards down 
    for (let i = startIndex; i < this.cards.length; i++) {
      // listener is required or the transition attribute will be left on all lower cards
      this.cards[i].addEventListener("transitionend", onAnimationEnd);
      this.cards[i].style.transform = `translateY(${gapSize}px)`;
    }
    
    // 2. request a transition on the lower cards to move up and fill the gap
    window.requestAnimationFrame(() => {
      for(let i = startIndex; i < this.cards.length; i++) {
        // stagger each cards transition delay
        this.cards[i].style.transition = `transform 250ms cubic-bezier(0,0,0.3,1) ${i*25}ms`;
        this.cards[i].style.transform = "";
      }
    });
  };
}

document.addEventListener("DOMContentLoaded", new Cards());