
/**
 * Usage:
 * const tabIndex = new TabIndex(element);
 * tabIndex.hidden = true;  // sets all focusable children of element to tabindex=-1
 * tabIndex.hidden = false; // restores all focusable children of element
 */

class TabIndex {
  constructor(element) {
    if (!element) {
      throw new Error("Missing required element argument.");
    }
    this._hidden = false;
    this._focusableElementsString = "a[href], area[href], input:not([disabled]), select:not([disabled]), \
      textarea:not([disabled]), button:not([disabled]), iframe, object, embed, [tabindex], [contenteditable]";
    this._focusableElements = Array.from(
      element.querySelectorAll(this._focusableElementsString)
    );
  }

  get hidden() {
    return this._hidden;
  }

  set hidden(isHidden) {
    if (this._hidden === isHidden) {
      return;
    }

    this._hidden = isHidden;

    this._focusableElements.forEach((child) => {
      if (isHidden) {
        // if the child has an explict tabindex save it
        if (child.hasAttribute("tabindex")) {
          child._savedTabindex = child.tabIndex;
        }
        // set focusable children to tabindex -1
        child.setAttribute("tabindex", -1);
        
        return;
      }
      
      if(!isHidden) {
        // if the child has a saved tabindex, restore it
        // since a valid value could be 0, explicitly check 0
        if (child._savedTabindex || child._savedTabindex === 0) {
          child.setAttribute("tabindex", child._savedTabindex);
        }
        else {
          // if there's nothing to restore, 
          // make sure there's no tabindex attribute
          child.removeAttribute("tabindex");
        }

        return;
      }
    });
  }
}
