
class SideNav {
  constructor() {
    this.showButton = document.querySelector(".header__side-nav-show");
    this.hideButton = document.querySelector(".side-nav__hide");
    this.sideNav = document.querySelector(".side-nav");
    this.sideNavDrawer = document.querySelector(".side-nav__drawer");

    this.tabIndex = new TabIndex(this.sideNavDrawer);
    this.tabIndex.hidden = true;

    this.startX = 0;
    this.currentX = 0;
    this.touchingSideNav = false;

    this.listenerOptions = this.passiveIfSupported();
    this.addEventListeners();
  }

  // apply passive event listening if supported
  // option defaults to true for touchstart and touchmove events on chrome and firefox
  passiveIfSupported = () => {
    let supportsPassive = false;

    try {
      document.addEventListener("test", null, 
        { 
          get passive() { 
            supportsPassive = { passive: true }; 
          }
        }
      );
    } catch (e) {}

    return supportsPassive;
  }

  addEventListeners = () => {
    this.showButton.addEventListener("click", this.showSideNav);
    this.hideButton.addEventListener("click", this.hideSideNav);
    this.sideNav.addEventListener("click", this.hideSideNav);
    this.sideNavDrawer.addEventListener("click", this.blockClicks);

    // passive option set to true signals that touchstart and 
    // touchmove will not call preventDefault()
    this.sideNav.addEventListener("touchstart", this.onTouchStart, this.listenerOptions);
    this.sideNav.addEventListener("touchmove", this.onTouchMove, this.listenerOptions);
    this.sideNav.addEventListener("touchend", this.onTouchEnd);
  }

  update = () => {
    if (!this.touchingSideNav) return;

    const translateX = Math.min(0, this.currentX - this.startX);
    this.sideNavDrawer.style.transform = `translateX(${translateX}px)`;

    // continue the animation until the touch has ended
    requestAnimationFrame(this.update);
  }

  showSideNav = () => {
    this.tabIndex.hidden = false;
    this.sideNav.classList.add("side-nav--visible");
  }

  hideSideNav = () => {
    this.tabIndex.hidden = true;
    this.sideNav.classList.remove("side-nav--visible");
  }

  blockClicks = (e) => {
    e.stopPropagation();
  }

  onTouchStart = (e) => {
    this.touchingSideNav = true;
    this.startX = e.touches[0].pageX;
    this.currentX = this.startX;

    // start the animation
    requestAnimationFrame(this.update);

    // console.log("touchstart:", this.startX);
  }

  onTouchMove = (e) => {
    if (!this.touchingSideNav) return;

    this.currentX = e.touches[0].pageX;

    // console.log("touchmove:", this.currentX);
  }

  onTouchEnd = (e) => {
    this.touchingSideNav = false;
    this.sideNavDrawer.style.transform = "";

    const translateX = Math.min(0, this.currentX - this.startX);
    if (translateX < 0) {
      this.hideSideNav();
    }

    // console.log("touchend:", this.currentX);
  }
}

new SideNav();